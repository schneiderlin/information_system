import sbt._

object Settings {
  val scalacOptions = Seq(
    "-feature",
    "-deprecation",
    "-unchecked",
    "-language:postfixOps",
    "-language:higherKinds"
  )

  object versions {
    val akkaVersion            = "2.6.3"
    val akkaHttpVersion        = "10.1.11"
    val monocleVersion         = "2.0.0"
    val zio                    = "1.0.0-RC17"
    val zioInteropCats         = "2.0.0.0-RC7"
    val catsVersion            = "2.0.0"
    val catsEffectVersion      = "2.1.1"
    val circeVersion           = "0.12.3"
    val enumeratumVersion      = "1.5.13"
    val enumeratumCirceVersion = "1.5.21"
    val elastic4sVersion       = "7.3.5"
  }

  val serverDependencies = Def.setting(
    Seq(
      // chimney
      "io.scalaland" %% "chimney" % "0.4.1",
      // zio
      "dev.zio" %% "zio"              % versions.zio,
      "dev.zio" %% "zio-interop-cats" % versions.zioInteropCats,
      // cats
      "org.typelevel" %% "cats-core"   % versions.catsVersion,
      "org.typelevel" %% "cats-effect" % versions.catsEffectVersion,
      // elastic4s
      "com.sksamuel.elastic4s" %% "elastic4s-core"          % versions.elastic4sVersion,
      "com.sksamuel.elastic4s" %% "elastic4s-client-esjava" % versions.elastic4sVersion,
      "com.sksamuel.elastic4s" %% "elastic4s-http-streams"  % versions.elastic4sVersion,
      "com.sksamuel.elastic4s" %% "elastic4s-effect-zio"    % versions.elastic4sVersion,
      "com.sksamuel.elastic4s" %% "elastic4s-json-circe"    % versions.elastic4sVersion,
      "com.sksamuel.elastic4s" %% "elastic4s-testkit"       % versions.elastic4sVersion % "test",
      // enum
      "com.beachape" %% "enumeratum"       % versions.enumeratumVersion,
      "com.beachape" %% "enumeratum-circe" % versions.enumeratumCirceVersion,
      // akka typed
      "com.typesafe.akka" %% "akka-actor-typed"         % versions.akkaVersion,
      "com.typesafe.akka" %% "akka-actor-testkit-typed" % versions.akkaVersion % Test,
      "com.typesafe.akka" %% "akka-stream"              % versions.akkaVersion,
      "com.typesafe.akka" %% "akka-http"                % versions.akkaHttpVersion,
      // json
      "de.heikoseeberger" %% "akka-http-circe" % "1.31.0",
      "io.circe"          %% "circe-core"      % versions.circeVersion,
      "io.circe"          %% "circe-generic"   % versions.circeVersion,
      "io.circe"          %% "circe-parser"    % versions.circeVersion,
      //"io.circe"          %% "circe-generic-extras" % versions.circeVersion,
      // monocle
      "com.github.julien-truffaut" %% "monocle-core"  % versions.monocleVersion,
      "com.github.julien-truffaut" %% "monocle-macro" % versions.monocleVersion,
      "com.github.julien-truffaut" %% "monocle-law"   % versions.monocleVersion % "test"
    )
  )

  val excludeDependencies = Def.setting(Seq("org.typelevel" % "scala-library"))
}
