import sbt.Keys._

// 基本配置
scalaVersion := "2.13.1"
scalacOptions in ThisBuild ++= Settings.scalacOptions

lazy val information_system = (project in file("."))
  .settings(
    addCompilerPlugin("org.typelevel"  %% "kind-projector"     % "0.11.0" cross CrossVersion.full),
    //addCompilerPlugin("org.scalamacros" %% "paradise"           % "2.1.1" cross CrossVersion.full),
    addCompilerPlugin("com.olegpy"      %% "better-monadic-for" % "0.3.0"),

    // 依赖
    libraryDependencies ++= Settings.serverDependencies.value,
    excludeDependencies ++= Settings.excludeDependencies.value,

    version in Docker := "latest",
    dockerExposedPorts in Docker := Seq(8080),
    dockerRepository := Some("670894261"),
    dockerBaseImage := "java"

    // 打包
    //assemblyMergeStrategy in assembly := {
    //  case "application.conf" ⇒ MergeStrategy.concat
    //  case "reference.conf" ⇒ MergeStrategy.concat
    //  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
    //  case _ => MergeStrategy.first
    //},

    // docker
    //dockerfile in docker := {
    //  // The assembly task generates a fat JAR file
    //  val artifact: File = assembly.value
    //  val artifactTargetPath = s"/app/${artifact.name}"
    //
    //  new Dockerfile {
    //    from("openjdk:8-jre")
    //    add(artifact, artifactTargetPath)
    //    //expose(8080)
    //    entryPoint("java", "-jar", artifactTargetPath)
    //  }
    //}
  )
  .enablePlugins(JavaAppPackaging)