package information.module.elasticsearch

import com.sksamuel.elastic4s.ElasticClient
import zio.Task

trait ElasticSearch {
  val elasticSearch: ElasticSearch.Service
}

object ElasticSearch {
  trait Service {
    def elasticClient: Task[ElasticClient]
  }
}