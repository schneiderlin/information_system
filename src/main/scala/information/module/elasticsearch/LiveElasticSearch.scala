package information.module.elasticsearch
import com.sksamuel.elastic4s.http.JavaClient
import com.sksamuel.elastic4s.{ElasticClient, ElasticProperties}
import zio.{Task, ZIO}

trait LiveElasticSearch extends ElasticSearch {
  override val elasticSearch: ElasticSearch.Service = new ElasticSearch.Service {
    override def elasticClient: Task[ElasticClient] =
      //ZIO(ElasticClient(JavaClient(ElasticProperties(s"http://127.0.0.1:9200"))))
      ZIO(ElasticClient(JavaClient(ElasticProperties(s"http://119.29.203.209:9200"))))
  }
}
