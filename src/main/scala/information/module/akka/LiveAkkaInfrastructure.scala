package information.module.akka

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.stream.Materializer
import zio.{Task, ZIO}

trait LiveAkkaInfrastructure extends AkkaInfrastructure {
  override val akkaInfrastructure: AkkaInfrastructure.Service = new AkkaInfrastructure.Service {
    override def akkaSystem: Task[ActorSystem[Nothing]] =
      ZIO(ActorSystem(Behaviors.empty, "actor_system"))

    override def materializer: Task[Materializer] =
      akkaSystem.map { system =>
        Materializer(system)
      }
  }
}
