package information.module.akka

import akka.actor.typed.ActorSystem
import akka.stream.Materializer
import zio.Task

trait AkkaInfrastructure {
  val akkaInfrastructure: AkkaInfrastructure.Service
}

object AkkaInfrastructure {
  trait Service {
    def akkaSystem: Task[ActorSystem[Nothing]]
    def materializer: Task[Materializer]
  }
}
