//package information
//import java.util.UUID
//
//import akka.actor.typed.ActorSystem
//import akka.actor.typed.scaladsl.Behaviors
//import akka.actor.typed.scaladsl.adapter._
//import akka.stream.Materializer
//import com.sksamuel.elastic4s.circe._
//import com.sksamuel.elastic4s.http.JavaClient
//import com.sksamuel.elastic4s.requests.searches.aggs.{AvgAggregation, TermsAggregation, TermsOrder}
//import com.sksamuel.elastic4s.requests.searches.queries.matches.MatchQuery
//import com.sksamuel.elastic4s.requests.searches.queries.{BoolQuery, ExistsQuery, RangeQuery}
//import com.sksamuel.elastic4s.zio.instances._
//import com.sksamuel.elastic4s.{ElasticClient, ElasticDsl, ElasticProperties}
//import information.model.{Note, NoteForm}
//import information.route.NoteRoute
//import io.circe.generic.auto._
//import io.scalaland.chimney.dsl._
//import zio.{App, DefaultRuntime, ZIO}
//
//object InformationApp extends App {
//  implicit val system  = ActorSystem(Behaviors.empty, "information")
//  implicit val mat     = Materializer(system)
//  implicit val classic = system.toClassic
//  implicit val ec      = system.executionContext
//  implicit val runtime = new DefaultRuntime {}
//
//  val client: ElasticClient = ElasticClient(JavaClient(ElasticProperties(s"http://127.0.0.1:9200")))
//  import ElasticDsl._
//
//  val allDocument = client.execute {
//    ElasticDsl
//      .search("note")
//      .matchAllQuery()
//  }
//
//  // 删掉所有不包含新字段的数据
//  val deleteAll = client.execute {
//    ElasticDsl
//      .deleteByQuery("note", BoolQuery().not(ExistsQuery("id")))
//  }
//
//  val customQuery = client.execute {
//    ElasticDsl
//      .search("bank")
//      .query(
//        BoolQuery()
//          .must(
//            MatchQuery("age", 40)
//          )
//          .not(
//            MatchQuery("state", "ID")
//          )
//          .filter(
//            RangeQuery("balance").gte(20000).lte(30000)
//          )
//      )
//  }
//
//  val aggregate = client.execute {
//    ElasticDsl
//      .search("bank")
//      .aggs(
//        TermsAggregation("group_by_state")
//          .field("state.keyword")
//          .order(TermsOrder("average_balance", asc = false))
//          .subAggregations(AvgAggregation("average_balance").field("balance")),
//      )
//  }
//
//  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
//    (for {
//      //allDocumentR <- allDocument
//      //r            = allDocumentR.result.to[NoteForm].toList
//      //_            <- ZIO.traverse(r)(note => NoteRoute.index(note.into[Note].withFieldConst(_.id, UUID.randomUUID().toString).transform, client))
//      _ <- deleteAll
//      _ <- zio.console.putStrLn("haha")
//      _ <- ZIO.never
//    } yield ())
//      .fold(err => { println(err.getMessage); 1 }, _ => 0)
//
//  //(for {
//  //  _ <- ZIO.fromFuture(_ => Http().bindAndHandle(route, "localhost", 8080))
//  //  _ <- zio.console.getStrLn
//  //} yield ())
//  //  .fold(_ => 1, _ => 0)
//}
