package information.model

case class Note(id: String, title: String, content: String)

case class NoteForm(title: String, content: String)