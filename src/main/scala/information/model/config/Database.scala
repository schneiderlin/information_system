package information.model.config

case class Database(
    className: String,
    url: String,
    user: String,
    password: String
)
