package information.model.config

final case class Application(
    server: Server,
    database: Database
)

object Application {
  val getConfig: Application = Application(
    Server("127.0.0.1", 8080),
    Database("jdbc:xxx", "127.0.0.1:3306", "test", "test")
  )
}
