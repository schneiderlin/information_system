package information.model.config

final case class Server(host: String, port: Int)
