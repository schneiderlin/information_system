package information.route

import java.util.UUID

import akka.http.scaladsl.server.Directives._
import com.sksamuel.elastic4s.circe._
import com.sksamuel.elastic4s.requests.searches.queries.BoolQuery
import com.sksamuel.elastic4s.requests.searches.queries.matches.MatchQuery
import com.sksamuel.elastic4s.{ElasticClient, ElasticDsl}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import information.implicits.ZIORoute._
import information.model.{Note, NoteForm}
import io.circe.generic.auto._
import zio.{Task, ZIO}
import io.scalaland.chimney.dsl._

class NoteRoute(client: ElasticClient) extends FailFastCirceSupport {
  import NoteRoute._

  lazy val route = pathPrefix("note") {
    addNote ~ queryNote ~ updateNote
  }

  val addNote = path("add") {
    (put & entity(as[NoteForm])) { form =>
      index(form.into[Note].withFieldConst(_.id, UUID.randomUUID().toString).transform, client)
        .map(complete(_))
    }
  }

  val queryNote = path("query") {
    (get & parameters("keyword")) { keyword =>
      query(keyword, client)
        .map(complete(_))
    }
  }

  val updateNote = path("update") {
    (patch & entity(as[Note])) { document =>
      updateById(document, client)
        .map(complete(_))
    }
  }
}

object NoteRoute {
  import ElasticDsl._

  def index(document: Note, client: ElasticClient): Task[Unit] =
    ZIO
      .fromFuture(_ =>
        client.execute {
          ElasticDsl
            .indexInto("note")
            .doc(document)
            .refreshImmediately
      })
      .map(_ => ())

  def query(keyword: String, client: ElasticClient): Task[List[Note]] =
    ZIO
      .fromFuture(_ =>
        client.execute {
          ElasticDsl
            .search("note")
            .query(
              BoolQuery()
                .should(
                  MatchQuery("content", keyword),
                  MatchQuery("title", keyword)
                )
            )
      })
      .map(_.result.safeTo[Note].filter(_.isSuccess).map(_.get))
      .map(_.toList)

  def updateById(note: Note, client: ElasticClient): Task[Unit] =
    ZIO
      .fromFuture(_ =>
        client.execute {
          ElasticDsl
            .updateById("note", note.id)
            .doc(note)
      })
      .unit
}
