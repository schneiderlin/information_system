package information

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.adapter._
import akka.actor.{ActorSystem => ClassicSystem}
import akka.http.scaladsl.Http
import akka.stream.Materializer
import information.model.config.Application
import information.module.akka.{AkkaInfrastructure, LiveAkkaInfrastructure}
import information.module.elasticsearch.{ElasticSearch, LiveElasticSearch}
import information.route.NoteRoute
import zio._
import zio.clock.Clock

object Main extends App {
  type AppEnvironment = Clock with AkkaInfrastructure with ElasticSearch

  def run(args: List[String]): ZIO[ZEnv, Nothing, Int] = {
    val applicationConfig = Application.getConfig
    val server = ZIO.runtime[AppEnvironment].flatMap { implicit rts =>
      for {
        implicit0(system: ActorSystem[Nothing]) <- rts.environment.akkaInfrastructure.akkaSystem
        implicit0(classicSystem: ClassicSystem) = system.toClassic
        implicit0(mater: Materializer)          <- rts.environment.akkaInfrastructure.materializer
        elasticClient                           <- rts.environment.elasticSearch.elasticClient
        route                                   = new NoteRoute(elasticClient).route
        _                                       <- ZIO.fromFuture(_ => Http().bindAndHandle(route, "0.0.0.0", 8080))
        _                                       <- ZIO(println("started"))
      } yield ()
    }
    val result: ZIO[zio.ZEnv, Throwable, Unit] = for {
      program <- server.provideSome[ZEnv] { base =>
                  new Clock with LiveAkkaInfrastructure with LiveElasticSearch {
                    override val clock: Clock.Service[Any] = base.clock
                  }
                }
      _ <- ZIO.never
    } yield program

    result
      .foldM(err =>
               for {
                 _ <- console.putStrLn(err.getMessage)
               } yield 1,
             _ => ZIO.succeed(0))
  }
}
