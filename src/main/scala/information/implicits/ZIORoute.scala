package information.implicits

import akka.http.scaladsl.server.{Route, RouteResult}
import zio.{DefaultRuntime, IO, Task, ZIO}

import scala.concurrent.Promise

object ZIORoute {
  val runtime: zio.Runtime[Any] = new DefaultRuntime {}

  implicit def zioRoute(program: Task[Route]): Route = ctx => {
    val p: Promise[RouteResult]       = Promise[RouteResult]()
    val f: IO[Throwable, RouteResult] = program.flatMap(r => ZIO.fromFunctionFuture(r)).provide(ctx)
    runtime.unsafeRunAsync(f) { exit =>
      exit.fold(e => p.failure(e.squash), s => p.success(s))
    }
    p.future
  }
}
