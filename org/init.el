;; 关掉顶部那个丑的一笔的gui图标栏
(tool-bar-mode -1)

;; 显示行号
(global-linum-mode 1)

;; 关闭启动帮助画面
(setq inhibit-splash-screen 1)

;; 快速打开配置文件
(defun open-init-file()
  (interactive)
  (find-file "~/.emacs.d/init.el"))

;; 绑定快速打开配置到F2
(global-set-key (kbd "<f2>") 'open-init-file)
